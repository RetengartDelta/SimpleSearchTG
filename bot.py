telegram_token = ""
yandex_disk_token = ""

import telebot
from yadisk import YaDisk
import docx2txt

def run_bot():
    # Устанавливаем токен вашего Telegram бота
    bot = telebot.TeleBot(telegram_token)

    y = YaDisk(token=yandex_disk_token)
    password = ""
    user_auth_state = {}

    # Обработка команды /start
    @bot.message_handler(commands=['start'])
    def start(message):
        if message.chat.id in user_auth_state and user_auth_state[message.chat.id]:
            bot.reply_to(message, "Вы уже аутентифицированы.\nИспользуйте команду /search и Ваше ФИО для поиска.\nПример: /search Иванов Иван Иванович")
        else:
            bot.reply_to(message, "Привет! Для доступа ко всем командам введите пароль.")

    # Обработка остальных команд
    @bot.message_handler(func=lambda message: True)
    def handle_command(message):
        if message.chat.id in user_auth_state and user_auth_state[message.chat.id]:
            # Пользователь уже аутентифицирован
            if message.text.startswith('/search'):
                # Получаем фразу из сообщения пользователя
                search_phrase = message.text.split('/search ')[1].strip()
                if search_phrase.count(' ') > 1:
                    try:
                        # Получаем список файлов .docx в указанной папке на Yandex Disk
                        files = y.listdir('disk:/')
                        docx_files = [file for file in files if file['name'].endswith('.docx')]
    
                        # Создаем пустой список для хранения найденных файлов
                        found_files = []
                        
                        # Обрабатываем каждый .docx файл
                        for file_info in docx_files:
                            # Скачиваем файл с Yandex Disk
                            file_path = f"file_{file_info['name']}"
                            y.download(file_info['path'], file_path)
                            
                            # Convert docx to txt
                            text = docx2txt.process(file_path)
                            
                            # Если фраза найдена, добавляем файл в список найденных файлов
                            if search_phrase in text:
                                found_files.append(file_info['name'][:-5])
                        
                        # Если найдены файлы, отправляем список их названий в ответ
                        if found_files:
                            bot.reply_to(message, f"Вы '{search_phrase}' найдены в следующих списках:\n\n{', '.join(found_files)}")
                        else:
                            bot.reply_to(message, f"Вы '{search_phrase}' не найдены в списках.")
                        
                    except Exception as e:
                        bot.reply_to(message, f"Ошибка при скачивании и поиске: {str(e)}")
                else:
                    bot.reply_to(message, f"Введите ФИО полностью!")

        else:
            # Пользователь еще не аутентифицирован
            if message.text == password:
                # Пользователь ввел правильный пароль
                user_auth_state[message.chat.id] = True
                bot.reply_to(message, "Доступ разрешен. Используйте команду /search и Ваше ФИО для поиска.\nПример: /search Иванов Иван Иванович")
            else:
                # Пользователь ввел неправильный пароль
                bot.reply_to(message, "Неверный пароль. Доступ запрещен.")

    # Запуск бота
    while True:
        try:
            bot.polling()
        except Exception as e:
            print(f"Ошибка при запуске бота: {str(e)}")
            continue

# Запуск бота в случае ошибки
if __name__ == '__main__':
    run_bot()
